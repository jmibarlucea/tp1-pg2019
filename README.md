# SCORELIB README #

Thank you for using this ScoreLib!

You can find the Wiki [here](https://bitbucket.org/jmibarlucea/tp1-pg2019/wiki/Home).
You'll find all the information you need for using ScoreLib over there.


### What is this repository for? ###

ScoreLib let's you have your own quick simple scoreboard.
You just set the manager, create a Score and add it to the ScoreManager and you're all set!
